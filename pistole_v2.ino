/*
  // March 2014 - TMRh20 - Updated along with High Speed RF24 Library fork
  // Parts derived from examples by J. Coliz <maniacbug@ymail.com>
*/
/**
 * Example for efficient call-response using ack-payloads 
 *
 * This example continues to make use of all the normal functionality of the radios including
 * the auto-ack and auto-retry features, but allows ack-payloads to be written optionlly as well.
 * This allows very fast call-response communication, with the responding radio never having to 
 * switch out of Primary Receiver mode to send back a payload, but having the option to if wanting
 * to initiate communication instead of respond to a commmunication.
 */
 


#include <SPI.h>
#include "nRF24L01.h"
#include "RF24.h"

#define DOBA_ZAPNUTI 10000
#define DOBA_PULSU 50 //v ms
uint64_t doba_zapnuti=0;

// Hardware configuration: Set up nRF24L01 radio on SPI bus plus pins 7 & 8 
RF24 radio(9,10);

// Topology
const uint64_t pipes[2] = { 0xABCDABCD71LL, 0x544d52687CLL };              // Radio pipe addresses for the 2 nodes to communicate.

// Role management: Set up role.  This sketch uses the same software for all the nodes
// in this system.  Doing so greatly simplifies testing.  

typedef enum { role_ping_out = 1, role_pong_back } role_e;                 // The various roles supported by this sketch
const char* role_friendly_name[] = { "invalid", "Ping out", "Pong back"};  // The debug-friendly names of those roles
role_e role = role_pong_back;                                              // The role of the current running sketch

// A single byte to keep track of the data being sent back and forth
byte counter = 1;
byte tlacitko = 0;
void setup(){
  pinMode(6, OUTPUT);
  digitalWrite(6,1);
  pinMode(7, OUTPUT);
  digitalWrite(7,0);
  

  Serial.begin(57600);

  Serial.print("\n\rNRF pistole pro strelnici\n\r");

  // Setup and configure rf radio

  radio.begin();
  radio.setAutoAck(1);                    // Ensure autoACK is enabled
  radio.enableAckPayload();               // Allow optional ack payloads
  radio.setRetries(0,15);                 // Smallest time between retries, max no. of retries
  radio.setPayloadSize(1);                // Here we are sending 1-byte payloads to test the call-response speed
  radio.openWritingPipe(pipes[1]);        // Both radios listen on the same pipes by default, and switch when writing
  radio.openReadingPipe(1,pipes[0]);
  radio.startListening();                 // Start listening
  //radio.printDetails();                   // Dump the configuration of the rf unit for debugging

  radio.openWritingPipe(pipes[0]);
  radio.openReadingPipe(1,pipes[1]);

  attachInterrupt(digitalPinToInterrupt(2),stisk,RISING);
  doba_zapnuti=millis()+DOBA_ZAPNUTI;
}

void loop(void) {
  if(tlacitko)
  {
    //cli();
    noInterrupts();
    tlacitko=0;
  
     radio.stopListening();                                  // First, stop listening so we can talk.
          
      Serial.print("Now sending ");
      Serial.print(counter,DEC);
      Serial.print(" as payload. ");
      byte gotByte;  
      unsigned long time = micros();                          // Take the time, and send it.  This will block until complete   
                                                              //Called when STANDBY-I mode is engaged (User is finished sending)
      if (!radio.write( &counter, 1 )){
        Serial.print("failed.\n\r");      
      }else{
  
        if(!radio.available()){ 
          Serial.print("Blank Payload Received\n\r"); 
        }else{
          while(radio.available() ){
            unsigned long tim = micros();
            radio.read( &gotByte, 1 );
            Serial.print("Got response %d\n\r");
            Serial.print(gotByte,DEC);
            counter++;
            if(gotByte==2)
            {
              digitalWrite(7,1);
              delay(DOBA_PULSU);
              digitalWrite(7,0);
            }
            else if(gotByte==4)
              digitalWrite(6,0);
          }
        }
      }
  doba_zapnuti=millis()+DOBA_ZAPNUTI;
  //sei();
  interrupts();
  }

if(doba_zapnuti<millis()) digitalWrite(6,0);
  
}

void stisk (void)
{
  tlacitko=1;
}
